const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const puzzle = require('./routes/api/puzzle');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api/puzzle', puzzle);


app.get('/', function (req, res) {
    res.send('I\'m a live');
})

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`server running on port : ${port}`));