const express = require('express');
const router = express.Router();
const mysql = require('mysql');

require('dotenv/config')

// const authMiddleware = (req, res, next) => true ? next() : res.sendStatus(401);

var con = mysql.createConnection({
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database,
});

const isConnect = true;

isConnect ? (
    con.connect(function (err) {
        if (err) throw err;
        console.log("Connected to PUZZLE!");
    })
) : null;

router.get('/mixedPuzzle', (req, res) => {
    let shufledBoard = [...Array(16).keys()].sort(() => (Math.random() > .5) ? 1 : -1);
    // let shufledBoard = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0, 14, 15]
    res.send(shufledBoard);
})

router.get('/winBoard', (req, res) => {

    let sql = 'SELECT user_name as name, moves, time FROM games ORDER BY moves ASC, time ASC LIMIT 10 ';

    con.query(sql, (err, result, fields) => {
        if (err) {
            res.status(500).send(err.sqlMessage)
        }
        res.send(result)
    })
})

router.post('/winResult', (req, res) => {
    const { user_name, moves, time } = req.body;

    var sql = "INSERT INTO `games` (user_name, moves, time) VALUES ?";
    var values = [
        [user_name, moves, time]
    ];
    con.query(sql, [values], function (err, result) {
        if (err) {
            res.status(500).json({ error: err });
        }
        if (result.affectedRows && result.affectedRows === 1) {
            res.status(201).json({});
        }
    })
});

module.exports = router;